# PruebaUniversidadCuba

Prueba a realizar por los estudiantes de la Universidad de Cuba para iniciar proceso de Curso de extension para aprendizaje de VeriTran Studio.

Es necesario que el equipo de computo que estén utilizando cuente con la herramienta git para descargar elcódigo y empezar a programar en él.

Para ello seguir los pasos de la página 
*https://git-scm.com/book/es/v1/Empezando-Instalando-Git*

En windows descargar la aplicación del siguiente enlace
*https://gitforwindows.org/*


**1. Descargar el código al equipo de computo**

`git clone https://gitlab.com/androsja/pruebauniversidadcuba.git`


Lo anterior para descargar el código utilizando una terminal o cmd en windows.

Al hacerlo solicitará usuario y password.

**2. El programa fue realizado en Android Studio, por tanto se recomienda tenerlo instalado, o tambien el Intelli J IDEA CE**

Android Studio puede ser descargado desde
*developer.android.com/AndroidStudio/Download*

**3. Importar código en Android Studio**

Utilizar la opción *Open a existing Android Studio project* para seleccionar lo descargado y generar un nuevo proyecto.


El nombre del proyecto creado debe ser MontoEscritoTest


**4. La prueba**

Es necesario ubicar la clase  MontoEscrito del paquete 'prueba.veritran.net.pruebauniversidad', 


Se requiere modificar el método getMontoEscrito el cual recibe un parámetro 'valor' entero, que puede estar entre 0 y 100.000.000.

Para que modifique la variable resultado y retorne en ella su montoescrito.

` public String getMontoEscrito(Integer valor){

        crearParseNumeros();
        if(parseoNumeros.containsKey(valor)){
            return  parseoNumeros.get(valor);
        }
        String resultado = "";

        return resultado;
    }`


*Ejemplo*

Si valor = 3450

resultado  = "tres mil cuatrocientos cincuenta"

Si valor = 1.704.031

resultado = "un millon setecientos cuatro mil treinta y uno"

**Consideraciones**

Debe utilizar la variable de clase *parseoNumeros* como parte de la solución propuesta.
La variable *parseaNumeros* cuenta ya con la definición de los montos numéricos mas relevantes que existen en el rango establecido.


**5. Clase de ayuda UniversityUnitTest**

Se ha creado la clase UniversityUnitTest del paquete 'prueba.veritran.net.pruebauniversidad'(test)

La cual puede ser utilizada para comprobar que su código va por buen camino.

En ella se encuentran seis pruebas que lo que hacen es verificar que getMontoEscrito esté retornando valores correctos. Sientase en confianza para añadir las pruebas que desee., ademas de las ya existentes.

Recordar que si todas las pruebas funcionan correctamente aparecerán seis iconos verdes de éxito al lado de cada prueba en la consola de Android Studio.


**6. Comandos para subir el código una vez esté finalizado**

`git commit -m "Escribir un mensaje en este espacio, que indique la razón del commit"`

`git push`


Para aprender mas sobre git recomendamos **https://rogerdudler.github.io/git-guide/index.es.html**

Para una mejor experiencia de usuario con git pueden utilizar el software sourcetree 

**https://es.atlassian.com/software/sourcetree**
