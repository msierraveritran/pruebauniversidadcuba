package prueba.veritran.net.pruebauniversidad;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UniversityUnitTest {
    MontoEscrito montoEscrito;
    @Before
    public void setup(){
        montoEscrito = new MontoEscrito();
    }
    @Test
    public void ceroTest() {
        assertEquals( "cero", montoEscrito.getMontoEscrito(0));
    }

    @Test
    public void nueveTest() {
        assertEquals( "nueve", montoEscrito.getMontoEscrito(9));
    }

    @Test
    public void milTest() {
        assertEquals("mil", montoEscrito.getMontoEscrito(1000));
    }

    @Test
    public void mil_1_Test() {
        assertEquals("nuevemil ciento cincuenta y seis", montoEscrito.getMontoEscrito(9156));
    }

    @Test
    public void millonTest() {
        assertEquals("un millon", montoEscrito.getMontoEscrito(1000000));
    }

    @Test
    public void millon_1_Test() {
        assertEquals( "tres millones doscientos noventa mil seiscientos cuarenta y uno", montoEscrito.getMontoEscrito(3290641));
    }

}